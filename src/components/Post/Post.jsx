import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getPostById } from "../../redux/actions/posts/postActions";
import { withRouter } from "react-router-dom";

class Post extends React.Component {

  componentDidMount() {
    const { match: { params }} = this.props
    this.props.getPostById(params.id)
  }
  
  render() {
    console.log(this.props.post);
    if (this.props.post) {
      return (
        <div className="PostWrapper">
          <h1>{this.props.post.title}</h1>
        </div>
      );
    }
    return <div className="PostWrapper"></div>;
  }

}

const mapStateToProps = (state) => {
  return {
    post: state.post,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getPostById,
    },
    dispatch
  );
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Post));
