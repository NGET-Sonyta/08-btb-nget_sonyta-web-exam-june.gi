import React, { useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getPosts, getPostById } from "../../redux/actions/posts/postActions";
import { Link } from "react-router-dom";

const ListPost = (props) => {
  useEffect(() => {
    props.getPosts();
  }, []);
  console.log(props.data);
  return (
    <div className="ListPostWrapper">
      {props.data.map((post) => {
        return (
          <React.Fragment key={post.id}>
            Post Id: {post.id}
            {" : "}
            <Link to={`/posts/${post.id}`}>{post.title}</Link>
            Description: {post.description}
            <br />
          </React.Fragment>
        );
      })}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getPosts,
      getPostById,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(ListPost);
